---
title: Hello, Blog
subtitle: "Building my blog"
date: "2022-02-21"
tags: [CI/CD]
type: post
---

**TL;DR** - Technically speaking, everything is explained here: [https://gitlab.com/mikefortunato/blog](https://gitlab.com/mikefortunato/blog)

I figured my first blog post could be about how this blog was created in the
first place. It's built with [Hugo](https://gohugo.io/), and uses
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and
[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) to build and deploy the blog.
